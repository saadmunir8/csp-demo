<?php
$webhookUrl = "https://webhook.site/3c5ed9b6-fee2-4252-9016-5865e0f4cce4"; //Endpoint for getting violation report
// Allow CORS preflight requests (OPTIONS)
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Origin: *'); // Allow requests from any origin
    header('Access-Control-Allow-Methods: POST'); // Allow POST requests
    header('Access-Control-Allow-Headers: Content-Type'); // Allow Content-Type header
    header('Access-Control-Max-Age: 86400'); // Cache preflight response for 24 hour

    // Convert the data to JSON format
    $jsonData = array(
        "message" => "Preflight request is received with the OPTIONS Request Method"
    );
    $jsonData = json_encode($jsonData);
    initiateRequest($webhookUrl, $jsonData);
    exit;
}

// Check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Get the JSON data from the request body
    $inputData = file_get_contents('php://input');
    $jsonData = json_decode($inputData, true);
    if ($jsonData !== null) {
        $jsonData = json_encode($jsonData);
        echo initiateRequest($webhookUrl, $jsonData);
    } else {
        // If JSON decoding failed, send an error response
        http_response_code(400); // Bad Request
        echo json_encode(array('error' => 'Invalid JSON data.'));
    }
} else {
    // If the request method is not POST, send an error response
    http_response_code(405); // Method Not Allowed
    echo json_encode(array('error' => 'Only POST requests are allowed.'));
}
function initiateRequest($url, $jsonData){
    $ch = curl_init($url);

    // Set cURL options
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData)
    ));
    $response = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'cURL error: ' . curl_error($ch);
    }
    curl_close($ch);
}
?>

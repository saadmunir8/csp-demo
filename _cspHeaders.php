<?php
// Set the Content Security Policy (CSP) header
header("Content-Security-Policy: default-src 'self'; font-src *; img-src https://www.google.com/; script-src 'self'; report-to csp-endpoint;");

// Set the Report-To header
$reportToHeader = [
    "group" => "csp-endpoint",
    "max_age" => 10886400,
    "endpoints" => [
        [
            "url" => "https://site.local/CSP-demo/reporting.php" //reporting endpoint
        ]
    ]
];

header("Report-To: " . json_encode($reportToHeader));
?>
